﻿; NewQTTabBarTab.ahk
;
; AutoHotkey script to use `Win` + e to create new tabs in QTTabBar.


; General configuration.
#SingleInstance force
Menu, Tray, Icon, NewQTTabBarTab.ico


; Opens a new tab in Windows Explorer using QTTabBar.
OpenNewTab() {
    SetTitleMatchMode, 2

    if (WinExist("ahk_class CabinetWClass")) {
        ; First and foremost, we need to bring the explorer windows to focus.
        if (not WinActive("ahk_class CabinetWClass")) {
            WinActivate ahk_class CabinetWClass
        }

        if (not WinExist("This PC")) {
            ; "This PC" special folder CLSID.
            ; @see https://www.thewindowsclub.com/the-secret-behind-the-windows-7-godmode
            ; If we are NOT in "This PC" in the current tab, a new tab is opened in "This PC".
            Run "::{20d04fe0-3aea-1069-a2d8-08002b30309d}"
            WinWait ahk_class CabinetWClass
        } else {
            ; If we are in "This PC", a "new tab" command is issued, cloning the actual tab.
            ; Thus, the new tab is opened in "This PC".
            SendInput ^n
        }
    } else {
        ; A new explorer window and tab is created, in "This PC".
        Run "::{20d04fe0-3aea-1069-a2d8-08002b30309d}"
        WinWait ahk_class CabinetWClass
        WinActivate

        ; Send Ctrl + l shortcut to lock the first tab.
        SendInput ^l
    }
}


; Captures the Win + e keystroke to handle new tabs in QTTabBar.
#e::OpenNewTab()